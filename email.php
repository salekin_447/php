<?php

    $header= "E M A I L";
    $h1 = "E M A I L H E A D E R";
    $h2 = "Lorem ipsum dolor sit amet consectetur.";
    $pp = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad dignissimos perferendis voluptas beatae pariatur ratione perspiciatis consectetur eligendi eveniet officia.";
    $div1 = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet soluta ipsam quas corrupti, nisi nemo quibusdam voluptatibus, iste molestia? Minima eos nam nesciunt ullam quos, voluptates aspernatur sequi ad praesentium est atque. Fugiat, eos!";
    $div2 = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet soluta ipsam quas corrupti, nisi nemo quibusdam voluptatibus, iste molestia? Minima eos nam nesciunt ullam quos, voluptates aspernatur sequi ad praesentium est atque. Fugiat, eos!";
    
    $value = <<< DATA
        <style>
            .header-box{
                background-color: wheat;
                width: 400px;
                height: 250px;
                padding-left:100px;
                padding-top :0;
                margin:auto;
            }
            .creating{
                padding-top: 120px;
                text-align: center;
            }
            .email{
                text-align: center;
            }
            .content{
                width: 380px;
                padding-left: 500px;
            }
            .mid-h2{
                font-size: 18px;
            }
            .pp{
                width: 350px
            }
            .grid{
                display: flex;
            
            }
            .div1{
                padding: 10px;
            }
            
        </style>
        <div>
            <div class="header-box">
                <P class = "creating">$header</P>
                <h1 class = "email">$h1</h1>
            </div>
            <div class="content">
                <h2 class="mid-h2">$h2</h2>
                <p class="pp">$pp</p>
            <div class="grid">
                <div class="div1">$div1</div>
                <div class="div1">$div2</div> 
            </div>
        </div>
        </div>
    DATA;
    

    echo $value;
?>